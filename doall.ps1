$ErrorActionPreference = "Stop"
which nasm
which make
conan test_package -s os=Windows -s build_type=Debug -s arch=x86 -s compiler="Visual Studio" -s compiler.runtime=MTd
conan test_package -s os=Windows -s build_type=Release -s arch=x86 -s compiler="Visual Studio" -s compiler.runtime=MT
conan test_package -s os=Windows -s build_type=Debug -s arch=x86_64 -s compiler="Visual Studio" -s compiler.runtime=MTd
conan test_package -s os=Windows -s build_type=Release -s arch=x86_64 -s compiler="Visual Studio" -s compiler.runtime=MT
