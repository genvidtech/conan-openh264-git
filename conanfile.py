from conans import ConanFile, tools, VisualStudioBuildEnvironment


class Openh264Conan(ConanFile):
    name = "openh264"
    version = "1.7.0"
    license = "MIT"
    description = "Conan Recipe for OpenH264 with support for Windows"
    url = "https://bitbucket.org/genvidtech/conan-openh264"
    settings = "os", "compiler", "build_type", "arch"
    options = {"shared": [True, False]}
    default_options = "shared=False"

    def source(self):
        self.run("git clone https://github.com/cisco/openh264.git")

    def build(self):
        make_args = 'libraries ARCH=%s' % self.settings.arch
        with tools.chdir("openh264") :
            if self.settings.compiler == "Visual Studio":
                make_args += ' OS=msvc'
                env_build = VisualStudioBuildEnvironment(self)
                with tools.environment_append(env_build.vars):
                    vcvars = tools.vcvars_command(self.settings)
                    self.run('%s && make %s' % (vcvars, make_args))
            else:
                self.run("make %s" % make_args)

    def package(self):
        self.copy("*.h", dst="include/wels", src="openh264/codec/api/svc")
        self.copy("openh264.lib", dst="lib", src="openh264", keep_path=False)
        self.copy("openh264_dll.lib", dst="lib", src="openh264", keep_path=False)
        self.copy("openh264.dll", dst="bin", src="openh264", keep_path=False)
        self.copy("*.so", dst="lib", src="openh264", keep_path=False)
        self.copy("*.so.*", dst="lib", src="openh264", keep_path=False)
        self.copy("*.a", dst="lib", src="openh264", keep_path=False)

    def package_info(self):
        self.cpp_info.libs = ["openh264"]
