#include <cstring>
#include "wels/codec_api.h"

int main() 
{
	int width = 1280;
	int height = 1024;
	float frameRate = 30.0f;
	int total_num = 1000;
	
	// Step 1: setup encoder
	ISVCEncoder * encoder = nullptr;
	int rv = WelsCreateSVCEncoder (&encoder);
	if(rv != 0 || encoder == nullptr)
		return -1;

	// Step 2: initilize with basic parameter
	SEncParamBase param;
	memset (&param, 0, sizeof (SEncParamBase));
	param.iUsageType = CAMERA_VIDEO_REAL_TIME;
	param.fMaxFrameRate = frameRate;
	param.iPicWidth = width;
	param.iPicHeight = height;
	param.iTargetBitrate = 5000000;
	encoder->Initialize (&param);
	
	// Step 3: set option, set option during encoding process
	int levelSetting = WELS_LOG_QUIET;
	encoder->SetOption (ENCODER_OPTION_TRACE_LEVEL, &levelSetting);
	int videoFormat = videoFormatI420;
	encoder->SetOption (ENCODER_OPTION_DATAFORMAT, &videoFormat);

	// Step 4: encode and  store ouput bistream
	int frameSize = width * height * 3 / 2;
	unsigned char * buf = new unsigned char[frameSize];
	if (buf == nullptr)
		return -1;
		
	SFrameBSInfo info;
	memset (&info, 0, sizeof (SFrameBSInfo));
	SSourcePicture pic;
	memset (&pic, 0, sizeof (SSourcePicture));
	pic.iPicWidth = width;
	pic.iPicHeight = height;
	pic.iColorFormat = videoFormatI420;
	pic.iStride[0] = pic.iPicWidth;
	pic.iStride[1] = pic.iStride[2] = pic.iPicWidth >> 1;
	pic.pData[0] = buf;
	pic.pData[1] = pic.pData[0] + width * height;
	pic.pData[2] = pic.pData[1] + (width * height >> 2);
	for(int num = 0;num < total_num; num++) {
		//prepare input data
		rv = encoder->EncodeFrame (&pic, &info);
		if(rv != cmResultSuccess)
			return -1;
		
		if (info.eFrameType != videoFrameTypeSkip) {
			//output bitstream
		}
	}
	
	// Step 5: teardown encoder
	if (encoder) {
		encoder->Uninitialize();
		WelsDestroySVCEncoder (encoder);
	}
	
	return 0;
}
