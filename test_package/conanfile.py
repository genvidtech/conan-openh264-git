from conans import ConanFile, CMake
import os


channel = os.getenv("CONAN_CHANNEL", "1.13.0")
username = os.getenv("CONAN_USERNAME", "genvidtech")


class Openh264TestConan(ConanFile):
    settings = "os", "compiler", "build_type", "arch"
    requires = "openh264/1.7.0@%s/%s" % (username, channel)
    generators = "cmake"

    def build(self):
        cmake = CMake(self)
        # Current dir is "test_package/build/<build_id>" and CMakeLists.txt is in "test_package"
        cmake.configure(source_dir=self.conanfile_directory, build_dir="./")
        cmake.build()

    def imports(self):
        self.copy("*.dll", "bin", "bin")
        self.copy("*.dylib", "bin", "bin")

    def test(self):
        os.chdir("bin")
        self.run(".%sexample" % os.sep)
